const path = require('path');
const webpack = require('webpack');

module.exports = {
  mode: "production",
  entry: "./src/core.js",
  output: {
    path: path.resolve(__dirname), 
    filename: "core.min.js", 
  },
  plugins: [
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery',
      'window.jQuery': 'jquery'
    }),
  ],
  module: {
    rules: [
      {
        test: /\.css?$/,
        use: ['style-loader', 'css-loader']
      },
    ]
}
}