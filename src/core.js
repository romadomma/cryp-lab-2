import './style.css';
const { decode, encode, scrambler, getBytesFrom} = require('./feistel-network');
const fs = window.require('fs');
import $ from 'jquery';

global.jQuery = global.$ = $;

//Global variables

const state = {
    inputText: null,
    outputText: null,
    inputKey: null,
    typeFn: null,
    typeKey: null,
    typeView: null,
    savedText: null,
    compareText: {
        first: '',
        second: '',
    }
};

const htmlElements = {
    inputText: $('#input-text'),
    outputText: $('#output-text'),
    inputKey: $('#input-key'),
    typeFn: $('input[name=type-fn]:radio'),
    typeKey: $('input[name=type-key]:radio'),
    typeView: $('input[name=type-view]:radio'),
    encode: $('#encode'),
    decode: $('#decode'),
    save: $('#save'),
    compare: $('#compare'),
    compareText: {
        first: $('#compare-text #compare-first'),
        second: $('#compare-text #compare-second'),
        label: $('#compare-label'),
    }
}

//Functions 
const init = () => {
    state.inputText = fs.readFileSync('./INPUT_TEXT.txt');
    state.inputKey = fs.readFileSync('./INPUT_KEY.txt');
    state.typeFn = htmlElements.typeFn.val();
    state.typeKey = htmlElements.typeKey.val();
    state.typeView = htmlElements.typeView.val();
    render();
}

const renderBuff = (buff) => {
    switch (state.typeView) {
        case 'utf8':
        case 'hex':
            return buff.toString(state.typeView);
        case 'binary':
            return buff.reduce((acc, v) => acc + v.toString(2).padStart(8, '0'), '');
    }
}

const render = () => ['inputText', 'outputText', 'inputKey']
    .map(v => state[v] ? htmlElements[v].val(renderBuff(state[v])) : null);

const compare = (first, second) => {

    if (first.length != second.length) {
        alert('Сообщения разной длинны невозможно сравнить');
        return;
    }
    first = renderBuff(first);
    second = renderBuff(second);
    state.compareText.first = '';
    state.compareText.second = '';
    let diffCount = 0;
    for (let i = 0; i < first.length; i++) {
        if (second[i] === first[i]) {
            state.compareText.first += first[i];
            state.compareText.second += second[i];
        } else {
            diffCount++;
            state.compareText.first += `<font class="diff">${first[i]}</font>`
            state.compareText.second += `<font class="diff">${second[i]}</font>`;
        }
    }
    htmlElements.compareText.label.text(`Результат сравнения. Отличий ${diffCount} из ${first.length} символов`);
    htmlElements.compareText.first.html(state.compareText.first);
    htmlElements.compareText.second.html(state.compareText.second);
}

const textToBuff = (text) => {
    switch (state.typeView) {
        case 'utf8':
        case 'hex':
            return window.Buffer.from(text, state.typeView);
        case 'binary':
            let blocks = [];
            for (let i = 0, j = text.length; i < j; i += 8)
                blocks.push(text.slice(i, i + 8));
            const a = blocks.reduce((acc, v) => {
                let res = 0x00;
                for (let i = 0; i < 8; i++) {
                    res = (res << 1) | (v[i] === '1' ? 0x01 : 0x00);
                }
                acc.push(res);
                return acc;
            }, []);
            return window.Buffer.from(a);
    }
}

//Events
htmlElements.inputText.on('change keyup', ({ target }) => {
    state.inputText = textToBuff(target.value);
});

htmlElements.inputKey.on('change keyup', ({ target }) => {
    state.inputKey = textToBuff(target.value);
});

htmlElements.typeKey.on('change', ({ target }) => {
    state.typeKey = target.value;
});

htmlElements.typeFn.on('change', ({ target }) => {
    state.typeFn = target.value;
});

htmlElements.typeView.on('change', ({ target }) => {
    state.typeView = target.value;
    render();
    if (state.compareText.first !== '' && state.compareText.second !== ''){
        console.log(1);
        compare(state.savedText, state.outputText);
    }
});

htmlElements.encode.on('click', (e) => {
    state.outputText = encode(window.Buffer.from(state.inputText), window.Buffer.from(state.inputKey), state.typeFn, state.typeKey);
    render();
    fs.writeFileSync('./OUTPUT_TEXT.txt', state.outputText);
});

htmlElements.decode.on('click', (e) => {
    state.outputText = decode(window.Buffer.from(state.outputText), window.Buffer.from(state.inputKey), state.typeFn, state.typeKey);
    render();
});

htmlElements.save.on('click', (e) => {
    state.savedText = state.outputText;
});

htmlElements.compare.on('click', (e) => {
    compare(state.savedText, state.outputText);
});

//Actions
init();