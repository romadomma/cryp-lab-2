const generateBite = (polynom, state, startBiteIndex, startByteIndex) => {
    let res = 0x00;
    let biteIndex = startBiteIndex;
    let byteIndex = startByteIndex;
    for (let i = polynom.length-1; i >= 0; i--)
        for (let j = 0; j < 8; j++) {
            if (biteIndex > 7) {
                biteIndex = 0;
                byteIndex--;
            }
            if (j == 0 && i == polynom.length-1)
                res = (polynom[i] & state[byteIndex]) & 0x01;
            else
                res = res ^ (((polynom[i] >> j) & (state[byteIndex] >> biteIndex)) & 0x01);
            biteIndex++;
        }
    return res;
};

const scrambler = (state, polynom, byteCount = 4) => {
    const needByte = byteCount - state.length;
    let biteIndex = 0;
    let byteIndex = needByte;
    if (needByte > 0)
        state = window.Buffer.concat([window.Buffer.alloc(needByte), state]);
    for (let i = needByte - 1; i >= 0; i--)
        for (let j = 0; j < 8; j++) {
            if (biteIndex > 7) {
                biteIndex = 0;
                byteIndex--;
            }
            state[i] = (state[i] << 1) | generateBite(polynom, state, biteIndex, byteIndex);
            biteIndex++;
        }
    return state;
};

const getBytesFrom = (key, ofset, length) => {
    let byteIndex = Math.floor(ofset / 8) % key.length;
    let bitIndex = ofset % 8;
    return window.Buffer.alloc(length).map(v => {
        for (let j = 0; j < 8; j++) {
            if (bitIndex > 7) {
                bitIndex = 0;
                byteIndex++;
                if (key.length <= byteIndex)
                    byteIndex = 0;
            }
            v = (v << 1) | ((key[byteIndex] >> (7-bitIndex)) & 0x01);
            bitIndex++;
        }
        return v;
    });
};

const feistelRound = (msg, v, fn) => ({
    first: fn(msg.first, v).map((item, index) => item ^ msg.second[index]),
    second: msg.first,
});

const feistelNetwork = (text, key, fnIndex, vIndex, stepCount, type = 'encode') => {
    let msg = {
        first: window.Buffer.from(text.slice(0, 4)),
        second: window.Buffer.from(text.slice(4, 8))
    };
    if (type === 'decode')
        for (let step = stepCount - 1; step >= 0; step--)
            msg = feistelRound(msg, vGenerator[vIndex](key, step), roundFn[fnIndex]);
    else
        for (let step = 0; step < stepCount; step++)
            msg = feistelRound(msg, vGenerator[vIndex](key, step), roundFn[fnIndex]);
    return window.Buffer.concat([msg.second, msg.first]);
}

const vGenerator = {
    0: (key, ofset) => getBytesFrom(key, ofset, 4),
    1: (key, ofset) => scrambler(getBytesFrom(key, ofset, 1), window.Buffer.from([0x03]), 4),
};

const roundFn = {
    0: (msg, v) => v,
    1: (msg, v) => scrambler(msg, window.Buffer.from([0x40, 0x03]), 4).map((item, index) => item ^ v[index])
};


const getBlocks = (text) => {
    let blocks = [];
    for (let i = 0, j = text.length; i < j; i += 8)
        blocks.push(text.slice(i, i + 8));
    return blocks;
};

module.exports = {
    decode: (text, key, fnIndex = 0, vIndex = 0, stepCount = 16) => {
        const res = getBlocks(text)
            .map(block => feistelNetwork(block, key, fnIndex, vIndex, stepCount, 'decode'))
            .reduce((acc, v) => window.Buffer.concat([acc, v]), window.Buffer.alloc(0));
        const lastByte = res.length - 1;
        if (res[lastByte] < 8 && res[lastByte] === res[lastByte - res[lastByte] + 1])
            return res.slice(0, lastByte - res[lastByte] + 1);
        return res;
    },
    encode: (text, key, fnIndex = 0, vIndex = 0, stepCount = 16) => {
        const needByte = 8 - (text.length % 8);
        if (needByte != 8)
            text = window.Buffer.concat([text, window.Buffer.alloc(needByte).fill(needByte)]);
        return getBlocks(text)
            .map(block => feistelNetwork(block, key, fnIndex, vIndex, stepCount, 'encode'))
            .reduce((acc, v) => window.Buffer.concat([acc, v]), window.Buffer.alloc(0))
    }
};