const path = require('path')
const {app, BrowserWindow} = require('electron')
  
  if (process.mas) app.setName('crypt_2')
  
  let mainWindow = null
  
  function initialize () {
    makeSingleInstance()
    
    function createWindow () {
      mainWindow = new BrowserWindow({
        width: 800,
        height: 500,
        autoHideMenuBar: true,
        title: app.getName(),
        webPreferences: {
          nodeIntegration: true
        }
      });
      mainWindow.loadURL(path.join('file://', __dirname, './src/core.html'))
      //mainWindow.webContents.openDevTools();
      mainWindow.setResizable(false);
      mainWindow.on('closed', () => {
        mainWindow = null
      })
    }
  
    app.on('ready', () => {
      createWindow()
    })
  
    app.on('window-all-closed', () => {
      if (process.platform !== 'darwin') {
        app.quit()
      }
    })
  
    app.on('activate', () => {
      if (mainWindow === null) {
        createWindow()
      }
    })
  }
  
  function makeSingleInstance () {
    if (process.mas) return
  
    app.requestSingleInstanceLock()
  
    app.on('second-instance', () => {
      if (mainWindow) {
        if (mainWindow.isMinimized()) mainWindow.restore()
        mainWindow.focus()
      }
    })
  }
  
  initialize()